/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;


import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.gaptap.bamboo.cloudfoundry.client.RouteRequestBuilder;
import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class RouteRequestBuilderTest {

    @Test
    public void hostAndDomain(){
        String route = "www.example.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);
        
        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("www"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void hostWithUnderscores(){
        String route = "www_something_else.example.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("www_something_else"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void domainWithUnderscores(){
        String route = "www.example_something_else.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example_something_else.com"));
        assertThat(request.getHost(), is("www"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void hostDomainAndPath(){
        String route = "some-thing.example.com/my-app";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("some-thing"));
        assertThat(request.getPath(), is("/my-app"));
    }
}
