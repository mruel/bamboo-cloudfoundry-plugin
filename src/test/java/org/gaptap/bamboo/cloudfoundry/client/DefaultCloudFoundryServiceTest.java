/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsRequest;
import org.cloudfoundry.client.v2.domains.DeleteDomainRequest;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.doppler.StreamRequest;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.DeleteApplicationRequest;
import org.cloudfoundry.operations.applications.GetApplicationEnvironmentsRequest;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.cloudfoundry.operations.applications.PushApplicationRequest;
import org.cloudfoundry.operations.applications.RenameApplicationRequest;
import org.cloudfoundry.operations.applications.StartApplicationRequest;
import org.cloudfoundry.operations.domains.Domain;
import org.cloudfoundry.operations.domains.Status;
import org.cloudfoundry.operations.routes.CreateRouteRequest;
import org.cloudfoundry.operations.routes.DeleteRouteRequest;
import org.cloudfoundry.operations.routes.ListRoutesRequest;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.cloudfoundry.operations.services.CreateServiceInstanceRequest;
import org.cloudfoundry.operations.services.GetServiceInstanceRequest;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultCloudFoundryServiceTest extends AbstractJavaClientTest{

    private CloudFoundryService service;
    private Logger logger = new Log4jLogger();

    private static void requestServiceInstanceNotFound(CloudFoundryOperations cloudFoundryOperations, String serviceInstanceName){
        when(cloudFoundryOperations
                .services()
                .getInstance(GetServiceInstanceRequest.builder()
                        .name(serviceInstanceName)
                        .build()))
                .thenReturn(Mono
                        .error(new IllegalArgumentException()));
    }

    private static void requestCreateServiceInstance(CloudFoundryOperations cloudFoundryOperations, ServiceConfiguration serviceConfiguration){
        when(cloudFoundryOperations
                .services()
                .createInstance(buildCreateServiceInstanceRequest(serviceConfiguration)))
                .thenReturn(Mono.empty());
    }

    private static CreateServiceInstanceRequest buildCreateServiceInstanceRequest(ServiceConfiguration serviceConfiguration) {
        return CreateServiceInstanceRequest.builder()
                .parameters(serviceConfiguration.getParameters())
                .tags(serviceConfiguration.getTags())
                .serviceInstanceName(serviceConfiguration.getServiceInstance())
                .serviceName(serviceConfiguration.getService())
                .planName(serviceConfiguration.getPlan())
                .build();
    }

    private static void requestListDomains(CloudFoundryOperations cloudFoundryOperations){
        List<Domain> domains = new ArrayList<>();
        domains.add(Domain.builder()
                .name("pivotal.io")
                .id("1")
                .status(Status.SHARED)
                .build());
        domains.add(Domain.builder()
                .name("libertymutual.com")
                .id("2")
                .status(Status.SHARED)
                .build());
        domains.add(Domain.builder()
                .name("davidehringer.com")
                .id("3")
                .status(Status.OWNED)
                .build());
        when(cloudFoundryOperations
                .domains().list())
                .thenReturn(Flux.fromIterable(domains));
    }

    private void requestDeleteDomain(CloudFoundryClient cloudFoundryClient, String domainId) {
        when(cloudFoundryClient.domains().delete(DeleteDomainRequest.builder()
                .domainId(domainId)
                .build()))
        .thenReturn(Mono.empty());
    }

    private void requestCreateRoute(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations.routes().create(any(CreateRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestDeleteRoute(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations.routes().delete(any(DeleteRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void pushApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().push(any(PushApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetApplication(CloudFoundryOperations cloudFoundryOperations){
        ApplicationDetail applicationDetail = ApplicationDetail.builder()
                .name("test-app")
                .id(UUID.randomUUID().toString())
                .stack("cflinuxfs2")
                .diskQuota(1073741824)
                .instances(2)
                .memoryLimit(1024)
                .requestedState("started")
                .runningInstances(2)
                .build();
        when(cloudFoundryOperations.applications().get(any(GetApplicationRequest.class)))
                .thenReturn(Mono.just(applicationDetail));
    }

    private void requestGetUnknownApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().get(any(GetApplicationRequest.class)))
                .thenReturn(Mono.error(new IllegalArgumentException()));
    }

    private void requestListServiceBindings(CloudFoundryClient cloudFoundryClient){
        when(cloudFoundryClient.applicationsV2().listServiceBindings(any(ListApplicationServiceBindingsRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetEnvironments(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().getEnvironments(any(GetApplicationEnvironmentsRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestListRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().list(any(ListRoutesRequest.class)))
                .thenReturn(Flux.empty());
    }

    private void requestStartApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().start(any(StartApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestMapRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().map(any(MapRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestUnmapRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().unmap(any(UnmapRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestStreamApplicationLogs(DopplerClient dopplerClient){
        when(dopplerClient.stream(any(StreamRequest.class)))
                .thenReturn(Flux.empty());
    }

    private void requestDeleteApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().delete(any(DeleteApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestRenameApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().rename(any(RenameApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void setupMocksForPush() {
        pushApplication(cloudFoundryOperations);
        requestGetApplication(cloudFoundryOperations);
        requestListServiceBindings(cloudFoundryClient);
        requestGetEnvironments(cloudFoundryOperations);
        requestListRoutes(cloudFoundryOperations);
        requestStartApplication(cloudFoundryOperations);
        requestMapRoutes(cloudFoundryOperations);
        requestUnmapRoutes(cloudFoundryOperations);
        requestStreamApplicationLogs(dopplerClient);
        requestDeleteApplication(cloudFoundryOperations);
        requestRenameApplication(cloudFoundryOperations);
    }

    @Before
    public final void setup() {
        Logger logger = new Log4jLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);
    }

    @Test
    public void pushNewService() throws InterruptedException {
        // Given
        ServiceConfiguration serviceConfiguration = ServiceConfiguration.builder()
                .plan("gold")
                .service("redis")
                .serviceInstance("my-redis")
                .build();

        requestServiceInstanceNotFound(cloudFoundryOperations, serviceConfiguration.getServiceInstance());
        requestCreateServiceInstance(cloudFoundryOperations, serviceConfiguration);

        // When
        StepVerifier
                .create(service.pushService(serviceConfiguration))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.services()).createInstance(buildCreateServiceInstanceRequest(serviceConfiguration));
    }

    @Test
    public void deleteNonExistentApplication() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.deleteApp("foo"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications(), never()).delete(any(DeleteApplicationRequest.class));
    }

    @Test
    public void deleteApplication() throws InterruptedException {
        // Given
        requestGetApplication(cloudFoundryOperations);
        requestDeleteApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.deleteApp("foo"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).delete(any(DeleteApplicationRequest.class));
    }

    @Test
    public void deleteDomain() throws InterruptedException {
        // Given
        requestListDomains(cloudFoundryOperations);
        requestDeleteDomain(cloudFoundryClient, "3");

        // when
        StepVerifier
                .create(service.deleteDomain("davidehringer.com"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryClient.domains()).delete(DeleteDomainRequest.builder()
                .domainId("3")
                .build());
    }

    @Test
    public void deleteNonExistentDomain() throws InterruptedException {
        // Given
        requestListDomains(cloudFoundryOperations);

        // when, then
        StepVerifier
                .create(service.deleteDomain("de.com"))
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void renameNonExistentApplicationFailIfNotExistTrue() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("non-existent", "new-name", true))
                .expectError();
    }

    @Test
    public void renameNonExistentApplicationFailIfNotExistFalse() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("non-existent", "new-name", false))
                .expectComplete()
                .verify();
    }

    @Test
    public void renameExistingApplication() throws InterruptedException {
        // Given
        requestGetApplication(cloudFoundryOperations);
        requestRenameApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("existent", "new-name", false))
                .expectComplete()
                .verify();

        // then
        verify(cloudFoundryOperations.applications()).rename(RenameApplicationRequest.builder()
                .name("existent")
                .newName("new-name")
                .build());
    }

    @Test
    public void createRouteDoesNotLogPathWhenItIsNotProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestCreateRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.createRoute("cloudfoundry.org", "bamboo", "", "space"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLog("Adding route bamboo.cloudfoundry.org..."));
        assertTrue(logger.containsInfoLog("Adding route bamboo.cloudfoundry.org... OK"));
    }

    @Test
    public void createRouteLogsThePathWhenItIsProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestCreateRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.createRoute("cloudfoundry.org", "bamboo", "/a-path", "space"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLog("Adding route bamboo.cloudfoundry.org/a-path..."));
        assertTrue(logger.containsInfoLog("Adding route bamboo.cloudfoundry.org/a-path... OK"));
    }

    @Test
    public void deleteRouteLogsThePathWhenItIsProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestDeleteRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.deleteRoute("cloudfoundry.org", "bamboo", "/a-path"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLog("Deleting route bamboo.cloudfoundry.org/a-path..."));
        assertTrue(logger.containsInfoLog("Deleting route bamboo.cloudfoundry.org/a-path... OK"));
    }

    @Test
    public void whenAHealthEndpointIsNotSpecifiedForABlueGreenDeploymentAHealthCheckIsNotExecuted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        when(healthChecker.assertHealthy(eq(applicationConfiguration), eq(blueGreenConfiguration), any(logger.getClass())))
                .thenReturn(Mono.empty());

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(healthChecker, never()).assertHealthy(eq(applicationConfiguration), eq(blueGreenConfiguration), any(logger.getClass()));
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheBlueGreenAppNameAndRoutesAreUsedForTheDarkApp() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("darkapp")
                .customDarkRoute("dark.cf.com")
                .build();

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).push(argThat(hasAppName(blueGreenConfiguration.customDarkAppName())));
        verify(cloudFoundryOperations.routes()).map(argThat(hasMapRoute(blueGreenConfiguration.customDarkAppName(), blueGreenConfiguration.customDarkRoute())));
    }

    @Test
    public void whenABlueGreenDeploymentUrlMappingFlow() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("darkapp")
                .customDarkRoute("dark.cf.com")
                .build();

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        ApplicationConfiguration darkApplicationConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        for(String route: applicationConfiguration.routes()) {
            verify(cloudFoundryOperations.routes()).map(argThat(hasMapRoute(blueGreenConfiguration.customDarkAppName(), route)));
        }
        for(String route: darkApplicationConfiguration.routes()){
            verify(cloudFoundryOperations.routes()).unmap(argThat(hasUnmapRoute(blueGreenConfiguration.customDarkAppName(), route)));
        }
    }

    private static ArgumentMatcher<PushApplicationRequest> hasAppName(String name){
        return new ArgumentMatcher<PushApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                PushApplicationRequest request = (PushApplicationRequest) o;
                return name.equals(request.getName());
            }
        };
    }

    private static ArgumentMatcher<MapRouteRequest> hasMapRoute(String appName, String route){
        return new ArgumentMatcher<MapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                MapRouteRequest request = (MapRouteRequest) o;
                return request.equals(RouteRequestBuilder.buildMapRouteRequest(appName, route));
            }
        };
    }

    private static ArgumentMatcher<UnmapRouteRequest> hasUnmapRoute(String appName, String route){
        return new ArgumentMatcher<UnmapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                UnmapRouteRequest request = (UnmapRouteRequest) o;
                return request.equals(RouteRequestBuilder.buildUnmapRouteRequest(appName, route));
            }
        };
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheOldAppIsDeleted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        // When
        StepVerifier.create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).delete(argThat(deleteRequestWithAppName("test-app")));
    }

    private static ArgumentMatcher<DeleteApplicationRequest> deleteRequestWithAppName(String name){
        return new ArgumentMatcher<DeleteApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                DeleteApplicationRequest request = (DeleteApplicationRequest) o;
                return name.equals(request.getName());
            }
        };
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheNewAppIsRenamedToTakeTheOldAppsName() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        // When
        StepVerifier.create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).rename(argThat(renameRequestWithAppNames("test-app-dark", "test-app")));
    }

    private static ArgumentMatcher<RenameApplicationRequest> renameRequestWithAppNames(String oldName, String newName){
        return new ArgumentMatcher<RenameApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                RenameApplicationRequest request = (RenameApplicationRequest) o;
                return oldName.equals(request.getName()) &&
                        newName.equals(request.getNewName());
            }
        };
    }

    @Test
    public void whenAHealthEndpointIsSpecifiedForABlueGreenDeploymentAHealthCheckIsExecuted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        when(healthChecker.assertHealthy(any(ApplicationConfiguration.class), any(BlueGreenConfiguration.class), any(logger.getClass())))
                .thenReturn(Mono.empty());

        // When
        StepVerifier.create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        ApplicationConfiguration convertedConfig = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        verify(healthChecker).assertHealthy(eq(convertedConfig), eq(blueGreenConfiguration), any(logger.getClass()));
    }

    @Test
    public void whenAHealthCheckFailsTheBlueGreenDeploymentIsAborted() throws InterruptedException {
        // Given
        pushApplication(cloudFoundryOperations);

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        when(healthChecker.assertHealthy(any(ApplicationConfiguration.class), any(BlueGreenConfiguration.class), any(logger.getClass())))
                .thenThrow(new HealthCheckException());

        // When
        boolean failed = false;
        try {

            StepVerifier.create(service.push(applicationConfiguration, new File("src/test/resources"), 60, blueGreenConfiguration))
                    .expectError(HealthCheckException.class)
                    .verify();
        } catch (HealthCheckException e){
            failed = true;
        }

        // Then
        assertTrue(failed);

        ApplicationConfiguration convertedConfig = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        verify(healthChecker).assertHealthy(eq(convertedConfig), eq(blueGreenConfiguration), any(logger.getClass()));
        verify(cloudFoundryOperations.routes(), never()).map(any(MapRouteRequest.class));
    }
}
