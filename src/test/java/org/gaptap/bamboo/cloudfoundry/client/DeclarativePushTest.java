/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsRequest;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsResponse;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationEnvironments;
import org.cloudfoundry.operations.applications.GetApplicationEnvironmentsRequest;
import org.cloudfoundry.operations.applications.PushApplicationRequest;
import org.cloudfoundry.operations.routes.ListRoutesRequest;
import org.cloudfoundry.operations.routes.Route;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DeclarativePushTest extends AbstractJavaClientTest {

    private AccumulatingLogger logger;
    private CloudFoundryService cloudFoundryService;
    private DeclarativePush push;

    @Before
    public void setup(){
        logger = new AccumulatingLogger();

        cloudFoundryService = mock(CloudFoundryService.class);
        push = new DeclarativePush(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, cloudFoundryService);
    }

    private void requestPush(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .applications()
                .push(any(PushApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetApp(CloudFoundryService cloudFoundryService){
        when(cloudFoundryService
                .getApp(anyString()))
                .thenReturn(Mono
                        .just(buildGenericApplicationDetail()));
    }

    private void requestApp(CloudFoundryService cloudFoundryService){
        when(cloudFoundryService
                .app(anyString()))
                .thenReturn(Mono
                        .just(buildGenericApplicationDetail()));
    }

    @NotNull
    private ApplicationDetail buildGenericApplicationDetail() {
        return ApplicationDetail.builder()
                .id("123")
                .name("unit-test")
                .stack("cflinuxfs2")
                .diskQuota(1024 * 1024)
                .instances(1)
                .memoryLimit(1024 * 1024)
                .requestedState("started")
                .runningInstances(1)
                .build();
    }

    private void requestListServiceBindings(CloudFoundryClient cloudFoundryClient) {
        when(cloudFoundryClient
                .applicationsV2()
                .listServiceBindings(any(ListApplicationServiceBindingsRequest.class)))
                .thenReturn(Mono.just(ListApplicationServiceBindingsResponse.builder()
                        .totalPages(1)
                        .totalResults(0)
                        .addAllResources(new ArrayList<>())
                        .build()));
    }

    private void requestEnvironment(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .applications()
                .getEnvironments(any(GetApplicationEnvironmentsRequest.class)))
                .thenReturn(Mono.just(ApplicationEnvironments.builder()
                        .build()));
    }

    private void requestListRoutes(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .routes()
                .list(any(ListRoutesRequest.class)))
                .thenReturn(Flux.<Route>empty());
    }

    private void requestMapRoute(CloudFoundryService cloudFoundryService) {
        when(cloudFoundryService.map(anyString(), anyString()))
                .thenReturn(Mono.empty());
    }

    private void noRouteTestMocks() {
        requestPush(cloudFoundryOperations);
        requestGetApp(cloudFoundryService);
        requestApp(cloudFoundryService);
        requestListServiceBindings(cloudFoundryClient);
        requestEnvironment(cloudFoundryOperations);
        requestListRoutes(cloudFoundryOperations);
        requestMapRoute(cloudFoundryService);
    }

    @Test
    @Ignore("This feature resulted in gettting 'InvalidRouteRelation' errors in some environments. Need to explore this use case more.")
    public void noRouteIsDisabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsNull() throws InterruptedException {
        // This will give your app a default Route, similar to the way the cli works

        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, new File("src/test/resources"), false, 180))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(false)));
    }

    @Test
    public void noRouteIsEnabledIfRoutesAreProvidedAndTheNoRoutesAttributeIsNull() throws InterruptedException {
        // Given
        noRouteTestMocks();

        List<String> routes = new ArrayList<>();
        routes.add("test.example.com");

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .addAllRoutes(routes)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, new File("src/test/resources"), false, 180))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(true)));
    }

    @Test
    public void noRouteIsEnabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsTrue() throws InterruptedException {
        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noRoute(true)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, new File("src/test/resources"), false, 180))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(true)));
    }

    @Test
    public void noRouteIsDisabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsFalse() throws InterruptedException {
        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noRoute(false)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, new File("src/test/resources"), false, 180))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(false)));
    }

    private static ArgumentMatcher<PushApplicationRequest> noRoute(boolean enabled){
        return new ArgumentMatcher<PushApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                PushApplicationRequest request = (PushApplicationRequest) o;
                return request.getNoRoute() == enabled;
            }
        };
    }

}
