/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskConfiguratorHelperImpl;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.struts.TextProvider;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.junit.Before;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ENVIRONMENT_CONFIG_OPTION_SHARED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.SPACE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.BaseCloudFoundryTaskConfigurator.TARGET_ID;
import static org.junit.Assert.assertTrue;

public class BaseCloudFoundryTaskConfiguratorTest {

    @Mock
    protected CloudFoundryAdminService adminService;
    @Mock
    protected TextProvider textProvider;
    @Mock
    protected BuildDefinitionManager buildDefinitionManager;
    @Mock
    protected EnvironmentService environmentService;
    @Mock
    protected EncryptionService encryptionService;
    @Mock
    protected CloudFoundryServiceFactory cloudFoundryServiceFactory;

    protected TaskConfiguratorHelper taskConfiguratorHelper;
    protected ErrorCollection errorCollection;

    @Before
    public final void setupBaseTest(){
        MockitoAnnotations.initMocks(this);

        taskConfiguratorHelper = new TaskConfiguratorHelperImpl(buildDefinitionManager, environmentService, textProvider);

        errorCollection = new SimpleErrorCollection();
    }

    protected Map<String, Object> getBaseRequiredParameters() {
        Map<String, Object> params = new HashMap<>();
        params.put(ENVIRONMENT_CONFIG_OPTION, ENVIRONMENT_CONFIG_OPTION_SHARED);
        params.put(TARGET_ID, "1");
        params.put(ORGANIZATION, "dehringer");
        params.put(SPACE, "sandbox");
        return params;
    }

    protected static void assertTaskValidationHasNoErrors(TaskConfigurator taskConfigurator, ActionParametersMap actionParametersMap, ErrorCollection errorCollection) {
        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertTrue(errorCollection.getErrors().isEmpty());
    }
}
