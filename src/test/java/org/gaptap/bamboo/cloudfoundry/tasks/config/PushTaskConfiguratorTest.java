package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_CONFIG_OPTION_YAML;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.APP_LOCATION_OPTION_DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_CUSTOM_DARK_CONFIG;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_GREEN_ENABLED;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_ENDPOINT;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DIRECTORY;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.DARK_APP_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.MONITOR;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SECONDS_TO_MONITOR;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.SELECTED_APP_CONFIG_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.START;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.PushTaskConfigurator.YAML_FILE;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;

public class PushTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest {

    private PushTaskConfigurator taskConfigurator;

    private Map<String, Object> params;
    private ActionParametersMap actionParametersMap;

    @Before
    public void setup(){
        taskConfigurator = new PushTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
        params.putAll(getRequiredPushTaskParameters());
        actionParametersMap = new SimpleActionParametersMap(params);
    }

    protected Map<String, Object> getRequiredPushTaskParameters() {
        Map<String, Object> params = new HashMap<>();
        params.put(SELECTED_APP_CONFIG_OPTION, APP_CONFIG_OPTION_YAML);
        params.put(YAML_FILE, "test.yml");
        params.put(APP_LOCATION, APP_LOCATION_OPTION_DIRECTORY);
        params.put(DIRECTORY, ".");
        params.put(START, "false");
        params.put(BLUE_GREEN_ENABLED, "false");
        return params;
    }

    @Test
    public void theBlueGreenEnabledFlagCanBeNullToSupportUpgradesFromVersionsBeforeItWasSupported(){
        params.remove(BLUE_GREEN_ENABLED);

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertTrue(errorCollection.getErrors().isEmpty());
    }

    @Test
    public void whenABlueGreenTaskIsEnabledAndCustomDarkConfigIsNotEnabledDarkAppNameAndRouteAreNotRequired(){
        params.put(BLUE_GREEN_ENABLED, "true");
        params.put(BLUE_GREEN_CUSTOM_DARK_CONFIG, "false");
        params.put(START, "true");
        params.put(MONITOR, "true");
        params.put(SECONDS_TO_MONITOR, "180");

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertTrue(errorCollection.getErrors().isEmpty());
    }

    @Test
    public void whenABlueGreenTaskIsEnabledAndCustomDarkConfigIsEnabledDarkAppNameAndRouteAreRequired(){
        params.put(BLUE_GREEN_ENABLED, "true");
        params.put(BLUE_GREEN_CUSTOM_DARK_CONFIG, "true");
        params.put(START, "true");
        params.put(MONITOR, "true");
        params.put(SECONDS_TO_MONITOR, "180");

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(DARK_APP_NAME));
        assertNotNull(errorCollection.getErrors().get(DARK_APP_ROUTE));

        errorCollection = new SimpleErrorCollection();
        params.put(DARK_APP_NAME, "green app");
        params.put(DARK_APP_ROUTE, "green.route.com");
        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertThat(errorCollection.getErrors().entrySet(), is(empty()));
    }

    @Test
    public void aBlueGreenDeploymentRequiresStartAndMonitorToBeSpecified(){
        params.put(BLUE_GREEN_ENABLED, "true");
        params.put(START, "false");
        params.put(MONITOR, "false");

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(START));
        assertNotNull(errorCollection.getErrors().get(MONITOR));
    }

    @Test
    public void blueGreenParametersAreSavedInTheTaskConfigMap(){
        params.put(BLUE_GREEN_ENABLED, "true");
        params.put(START, "true");
        params.put(MONITOR, "true");
        params.put(SECONDS_TO_MONITOR, "180");
        params.put(DARK_APP_NAME, "green app");
        params.put(DARK_APP_ROUTE, "green.route.com");
        params.put(BLUE_HEALTH_CHECK_ENDPOINT, "/health");
        params.put(BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION, "true");
        params.put(BLUE_GREEN_CUSTOM_DARK_CONFIG, "true");


        TaskDefinition previousTaskDefinition = mock(TaskDefinition.class, RETURNS_SMART_NULLS);
        Map<String, String> taskConfigMap = taskConfigurator.generateTaskConfigMap(actionParametersMap, previousTaskDefinition);

        assertThat(taskConfigMap.get(BLUE_GREEN_ENABLED), is("true"));
        assertThat(taskConfigMap.get(DARK_APP_NAME), is("green app"));
        assertThat(taskConfigMap.get(DARK_APP_ROUTE), is("green.route.com"));
        assertThat(taskConfigMap.get(BLUE_HEALTH_CHECK_ENDPOINT), is("/health"));
        assertThat(taskConfigMap.get(BLUE_HEALTH_CHECK_SKIP_SSL_VALIDATION), is("true"));
        assertThat(taskConfigMap.get(BLUE_GREEN_CUSTOM_DARK_CONFIG), is("true"));
    }
}
