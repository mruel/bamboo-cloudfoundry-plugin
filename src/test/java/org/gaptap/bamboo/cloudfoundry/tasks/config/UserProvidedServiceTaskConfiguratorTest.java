package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.collections.SimpleActionParametersMap;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.DATA_OPTION_INLINE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.INLINE_DATA;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.REQUEST_FILE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SELECTED_DATA_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.UserProvidedServiceTaskConfigurator.SERVICE_NAME;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

public class UserProvidedServiceTaskConfiguratorTest extends BaseCloudFoundryTaskConfiguratorTest {

    private UserProvidedServiceTaskConfigurator taskConfigurator;

    private Map<String, Object> params;
    private ActionParametersMap actionParametersMap;

    @Before
    public void setup(){
        taskConfigurator = new UserProvidedServiceTaskConfigurator(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);

        params = getBaseRequiredParameters();
        actionParametersMap = new SimpleActionParametersMap(params);
    }

    private String fileAsString(String fileName){
        String filename = "user-provided-services/" + fileName;
        ClassPathResource resource = new ClassPathResource(filename);
        try {
            return FileCopyUtils.copyToString(new InputStreamReader(resource.getInputStream()));
        } catch (IOException e) {
            throw new RuntimeException("Test setup error: unable to find test resources file with name " + filename, e);
        }

    }

    @Test
    public void theServiceNameFieldAcceptsBambooVariables(){
        params.put(SERVICE_NAME, "${bamboo.serviceName}");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("simple-credentials.json"));

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void invalidInlineJsonResultsInAValidationError(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("invalid-credentials.json"));

        taskConfigurator.validate(actionParametersMap, errorCollection);

        assertFalse(errorCollection.getErrors().isEmpty());
        assertNotNull(errorCollection.getErrors().get(INLINE_DATA));
    }

    @Test
    public void theInlineJsonDataAcceptsABambooVariableAsTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, "${bamboo.jsonContent}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theInlineJsonDataAcceptsEmbededBambooVariables(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_INLINE);
        params.put(INLINE_DATA, fileAsString("credentials-with-variables.json"));

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theJsonFileNameDataAcceptsABambooVariableAsTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_FILE);
        params.put(REQUEST_FILE, "${bamboo.fileName}");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }

    @Test
    public void theJsonFileNameDataAcceptsBambooVariableEmbeddedInTheValue(){
        params.put(SERVICE_NAME, "test-service");
        params.put(SELECTED_DATA_OPTION, DATA_OPTION_FILE);
        params.put(REQUEST_FILE, "environment-${bamboo.fileName}.json");

        assertTaskValidationHasNoErrors(taskConfigurator, actionParametersMap, errorCollection);
    }
}
