
@Value.Style(
        add = "*",
        defaults = @Value.Immutable(copy = false),
        depluralize = true,
        put = "*",
        typeAbstract = "_*",
        typeImmutable = "*",
        visibility = ImplementationVisibility.PUBLIC
)
package org.gaptap.bamboo.cloudfoundry;

import org.immutables.value.Value;
import org.immutables.value.Value.Style.ImplementationVisibility;