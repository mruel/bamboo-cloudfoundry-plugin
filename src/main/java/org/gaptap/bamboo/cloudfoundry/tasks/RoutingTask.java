/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.DOMAIN_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.HOST_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.HOST_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_ADD_DOMAIN;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_ADD_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_DELETE_DOMAIN;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.OPTION_DELETE_ROUTE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.ORGANIZATION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.PATH_ROUTE_ADD;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.PATH_ROUTE_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.RoutingTaskConfigurator.SPACE;

/**
 * @author David Ehringer
 */
public class RoutingTask extends AbstractCloudFoundryTask {

    public RoutingTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    public TaskResult doExecute(CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();

        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        try {
            if (OPTION_ADD_DOMAIN.equals(option)) {
                buildLogger.addBuildLogEntry("Creating private domain " + configMap.get(DOMAIN_ADD) + " " + getLoginContext(taskContext));
                addDomain(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_DELETE_DOMAIN.equals(option)) {
                buildLogger.addBuildLogEntry("Deleting domain " + configMap.get(DOMAIN_DELETE) + " " + getLoginContext(taskContext));
                deleteDomain(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_ADD_ROUTE.equals(option)) {
                buildLogger.addBuildLogEntry("Creating route " + getRouteToAdd(configMap) + " " + getLoginContext(taskContext));
                addRoute(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_DELETE_ROUTE.equals(option)) {
                buildLogger.addBuildLogEntry("Deleting route " + getRouteToDelete(configMap) + " " + getLoginContext(taskContext));
                deleteRoute(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else {
                throw new TaskException("Unknown or unspecified routing management option: " + option);
            }
        } catch (InterruptedException e) {
            buildLogger.addErrorLogEntry("Unable to complete task due to unknown error: " + e.getMessage());
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    private String getRouteToAdd(ConfigurationMap configMap) {
        return String.format("%s.%s%s", configMap.get(HOST_ROUTE_ADD), configMap.get(DOMAIN_ROUTE_ADD), configMap.get(PATH_ROUTE_ADD));
    }

    private String getRouteToDelete(ConfigurationMap configMap) {
        return String.format("%s.%s%s", configMap.get(HOST_ROUTE_DELETE), configMap.get(DOMAIN_ROUTE_DELETE), configMap.get(PATH_ROUTE_DELETE));
    }

    private void addDomain(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                           TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String domain = configMap.get(DOMAIN_ADD);
        doSubscribe(cloudFoundry.addDomain(domain, configMap.get(ORGANIZATION)), "Unable to create private domain.", buildLogger, taskResultBuilder);
    }

    private void deleteDomain(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                              TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String domain = configMap.get(DOMAIN_DELETE);
        doSubscribe(cloudFoundry.deleteDomain(domain), "Unable to delete private domain.", buildLogger, taskResultBuilder);
    }

    private void addRoute(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                          TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String domain = configMap.get(DOMAIN_ROUTE_ADD);
        String host = configMap.get(HOST_ROUTE_ADD);
        String path = configMap.get(PATH_ROUTE_ADD);
        String space = configMap.get(SPACE);
        doSubscribe(cloudFoundry.createRoute(domain, host, path, space), "Unable to create route.", buildLogger, taskResultBuilder);
    }

    private void deleteRoute(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap,
                             TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String domain = configMap.get(DOMAIN_ROUTE_DELETE);
        String host = configMap.get(HOST_ROUTE_DELETE);
        String path = configMap.get(PATH_ROUTE_DELETE);
        doSubscribe(cloudFoundry.deleteRoute(domain, host, path), "Unable to delete route.", buildLogger, taskResultBuilder);
    }

}
