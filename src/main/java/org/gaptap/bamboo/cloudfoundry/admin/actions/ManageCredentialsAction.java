/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin.actions;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.atlassian.bamboo.security.EncryptionService;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.admin.Credentials;
import org.gaptap.bamboo.cloudfoundry.admin.InUseByTargetException;
import org.gaptap.bamboo.cloudfoundry.admin.Target;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 */
@SuppressWarnings("serial")
public class ManageCredentialsAction extends BaseManagementAction {

	private int credentialsId;
	private String username;
	private String password;
	private String name;
	private String description;
	private String changePassword;

	private List<Target> targetsUsingCredentials = Lists.newArrayList();

	private final CloudFoundryAdminService adminService;
	private final EncryptionService encryptionService;

	public ManageCredentialsAction(CloudFoundryAdminService adminService, EncryptionService encryptionService) {
		this.adminService = adminService;
		this.encryptionService = encryptionService;
	}

	public String doCreate() throws Exception {
		if (!isValidCredentails()) {
			setMode(ADD);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		adminService.addCredentials(name, username, encryptionService.encrypt(password), description);
		return SUCCESS;
	}

	public String doEdit() {
		setMode(EDIT);
		Credentials credentials = adminService.getCredentials(credentialsId);
		name = credentials.getName();
		username = credentials.getUsername();
		password = credentials.getPassword();
		description = credentials.getDescription();
		return EDIT;
	}

	public String doUpdate() throws Exception {
		if (!isValidCredentails()) {
			setMode(EDIT);
			addActionError(getText("cloudfoundry.global.errors.task.validation.errors"));
			return ERROR;
		}
		String encryptedPassword = null;
		if ("true".equals(changePassword)) {
			encryptedPassword = encryptionService.encrypt(password);
		} else {
			Credentials credentials = adminService.getCredentials(credentialsId);
			encryptedPassword = credentials.getPassword();
		}

		// TODO check that all CF targets are still valid (e.g. login)?

		// TODO detect issues
		adminService.updateCredentials(credentialsId, name, username, encryptedPassword, description);
		return SUCCESS;
	}

	private boolean isValidCredentails() {
		// TODO extract strings
		// TODO addition validation?
		if (StringUtils.isBlank(getName())) {
			addFieldError("name", getText("cloudfoundry.global.required.field"));
		}
		if (StringUtils.isBlank(getUsername())) {
			addFieldError("username", getText("cloudfoundry.global.required.field"));
		}
		if ("true".equals(changePassword) && StringUtils.isBlank(getPassword())) {
			addFieldError("password", getText("cloudfoundry.global.required.field"));
		}
		return !(hasFieldErrors() || hasActionErrors());
	}

	public String doDelete() {
		try {
			adminService.deleteCredentials(credentialsId);
		} catch (InUseByTargetException e) {
			targetsUsingCredentials.addAll(e.getTargetsUsing());
			// provide Credentials data to display back on error page
			doEdit();
			return ERROR;
		}
		return SUCCESS;
	}

	public Collection<Credentials> getCredentials() {
		return adminService.allCredentials();
	}

	public int getCredentialsId() {
		return credentialsId;
	}

	public void setCredentialsId(int credentialsId) {
		this.credentialsId = credentialsId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getChangePassword() {
		return changePassword;
	}

	public void setChangePassword(String changePassword) {
		this.changePassword = changePassword;
	}

	public List<Target> getTargetsUsingCredentials() {
		return targetsUsingCredentials;
	}

	public void setTargetsUsingCredentials(List<Target> targetsUsingCredentials) {
		this.targetsUsingCredentials = targetsUsingCredentials;
	}

}
