/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.domains.DeleteDomainRequest;
import org.cloudfoundry.client.v2.info.GetInfoRequest;
import org.cloudfoundry.client.v2.info.GetInfoResponse;
import org.cloudfoundry.client.v2.services.ListServicesRequest;
import org.cloudfoundry.client.v2.services.ServiceEntity;
import org.cloudfoundry.client.v2.services.ServiceResource;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.cloudfoundry.operations.applications.DeleteApplicationRequest;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.cloudfoundry.operations.applications.RenameApplicationRequest;
import org.cloudfoundry.operations.applications.RestartApplicationRequest;
import org.cloudfoundry.operations.applications.StartApplicationRequest;
import org.cloudfoundry.operations.applications.StopApplicationRequest;
import org.cloudfoundry.operations.domains.CreateDomainRequest;
import org.cloudfoundry.operations.routes.CreateRouteRequest;
import org.cloudfoundry.operations.routes.DeleteRouteRequest;
import org.cloudfoundry.operations.services.BindServiceInstanceRequest;
import org.cloudfoundry.operations.services.CreateServiceInstanceRequest;
import org.cloudfoundry.operations.services.CreateUserProvidedServiceInstanceRequest;
import org.cloudfoundry.operations.services.DeleteServiceInstanceRequest;
import org.cloudfoundry.operations.services.GetServiceInstanceRequest;
import org.cloudfoundry.operations.services.ServiceInstance;
import org.cloudfoundry.operations.services.UnbindServiceInstanceRequest;
import org.cloudfoundry.operations.services.UpdateServiceInstanceRequest;
import org.cloudfoundry.util.PaginationUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple3;

import java.io.File;
import java.time.Duration;
import java.util.Map;

import static org.cloudfoundry.util.tuple.TupleUtils.function;

/**
 * @author David Ehringer
 * 
 */
public class DefaultCloudFoundryService implements CloudFoundryService {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(DefaultCloudFoundryService.class);

    private final CloudFoundryOperations cloudFoundryOperations;
    private final CloudFoundryClient cloudFoundryClient;
    private final DopplerClient dopplerClient;

    private final Logger logger;
    private final HealthChecker healthChecker;

    public DefaultCloudFoundryService(CloudFoundryOperations cloudFoundryOperations, CloudFoundryClient cloudFoundryClient,
                                      DopplerClient dopplerClient, Logger logger, HealthChecker healthChecker) {
        this.cloudFoundryOperations = cloudFoundryOperations;
        this.cloudFoundryClient = cloudFoundryClient;
        this.dopplerClient = dopplerClient;
        this.logger = logger;
        this.healthChecker = healthChecker;
    }

    @Override
    public Mono<Void> validateConnection() {
        // timeout is a workaround for https://github.com/cloudfoundry/cf-java-client/issues/562
        return cloudFoundryOperations
                .buildpacks()
                .list()
                .timeout(Duration.ofSeconds(5))
                .then();
    }

    @Override
    public Mono<GetInfoResponse> info() {
        return cloudFoundryClient
                .info()
                .get(GetInfoRequest.builder()
                        .build())
                .doOnSubscribe(subscription -> logger.info("Getting CloudInfo..."))
                .doOnSuccess(response -> logger.info("Getting CloudInfo... OK"))
                .doOnError(throwable -> logger.info("Getting CloudInfo... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Flux<ApplicationSummary> apps() {
        return cloudFoundryOperations
                .applications()
                .list()
                .doOnSubscribe(subscription -> logger.info("Getting applications..."))
                .doOnComplete(() -> logger.info("Getting applications.. OK"))
                .doOnError(throwable -> logger.error("Getting applications... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Mono<ApplicationDetail> app(String name) {
        return getApp(name)
                .doOnSubscribe(it -> logger.info(String.format("Getting application %s...", name)))
                .doOnSuccess(app -> logger.info(String.format("Getting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Getting application %s... FAILED: %s", name, error.getMessage())));
    }

    public Mono<ApplicationDetail> getApp(String name) {
        return cloudFoundryOperations
                .applications()
                .get(GetApplicationRequest.builder()
                        .name(name)
                        .build());
    }

    @Override
    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, boolean start) {
        return push(applicationConfiguration, application, start, null);
    }

    @Override
    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, int secondsToWait) {
        return push(applicationConfiguration, application, true, secondsToWait);
    }

    @Override
    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, int secondsToWait, BlueGreenConfiguration blueGreenConfiguration) {
        ApplicationConfiguration darkAppConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        return push(darkAppConfiguration, application, true, secondsToWait)
                .then(performOptionalHealthCheck(darkAppConfiguration, blueGreenConfiguration))
                .thenMany(mapLiveUrlsToDarkApp(darkAppConfiguration, applicationConfiguration))
                .thenMany(unMapDarkUrlsFromDarkApp(darkAppConfiguration))
                .thenEmpty(deleteOldApp(applicationConfiguration))
                .then(renameApp(darkAppConfiguration.name(), applicationConfiguration.name()))
                .then(app(applicationConfiguration.name()))
                .doOnSubscribe(subscription -> logger.info("Starting blue/green deployment"))
                .doOnSuccess(applicationDetail -> logger.info("Blue/green deployment successfully completed."))
                .doOnError(throwable -> {
                    logger.error("Blue/green deployment failed: " + throwable.getMessage());
                    LOG.error("Blue/green deployment failed: " + throwable.getMessage(), throwable);
                });
    }

    private Mono<Void> performOptionalHealthCheck(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration) {
        if(blueGreenConfiguration.healthCheckEnabled()){
            return healthChecker.assertHealthy(applicationConfiguration, blueGreenConfiguration, logger);
        }
        return Mono.empty();
    }

    private Flux<Void> mapLiveUrlsToDarkApp(ApplicationConfiguration darkApplicationConfiguration, ApplicationConfiguration applicationConfiguration) {
        return Flux.fromIterable(applicationConfiguration.routes())
                .flatMap(route -> map(darkApplicationConfiguration.name(), route), 1);
    }

    private Flux<Void> unMapDarkUrlsFromDarkApp(ApplicationConfiguration applicationConfiguration) {
        return unMapUrlsFromApp(applicationConfiguration);
    }

    private Flux<Void> unMapLiveUrlsFromOldApp(ApplicationConfiguration applicationConfiguration) {
        return unMapUrlsFromApp(applicationConfiguration);
    }

    private Flux<Void> unMapUrlsFromApp(ApplicationConfiguration applicationConfiguration) {
        return Flux.fromIterable(applicationConfiguration.routes())
                .flatMap(route -> Mono.when(
                        Mono.just(route),
                        getApp(applicationConfiguration.name())))
                .flatMap(function((route, applicationDetail) -> unmap(applicationConfiguration.name(), route)), 1);
    }


    private Mono<Void> deleteOldApp(ApplicationConfiguration applicationConfiguration) {
        return deleteApp(applicationConfiguration.name());
    }

    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, boolean start, Integer timeout){
        DeclarativePush declarativePush = new DeclarativePush(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, this);
        return declarativePush.push(applicationConfiguration, application, start, timeout);
    }

    @Override
    public Mono<Void> startApp(String name) {
        return cloudFoundryOperations
                .applications()
                .start(StartApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(it -> logger.info(String.format("Starting application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Starting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Starting application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> startApp(String name, int secondsToWait) {
        // TODO how to handle secondsToWait? with timeout?
        return startApp(name);
    }

    @Override
    public Mono<Void> stopApp(String name) {
        return cloudFoundryOperations
                .applications()
                .stop(StopApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Stopping application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Stopping application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Stopping application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> restartApp(String name) {
        return cloudFoundryOperations
                .applications()
                .restart(RestartApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Restarting application %s...", name)))
                .doOnSuccess(aVoid -> logger.info(String.format("Restarting application %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Restarting application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> deleteApp(String name) {
        return getApp(name)
                .then(app -> doDelete(name))
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting application %s...", name)))
                .otherwise(Exception.class, e -> {
                    logger.info(String.format("Application %s does not exist", name));
                    return Mono.empty();
                })
                .doOnSuccess(aVoid -> logger.info(String.format("Deleting application %s... OK", name)));
    }

    private Mono<Void> doDelete(String name) {
        return cloudFoundryOperations
                .applications()
                .delete(DeleteApplicationRequest.builder()
                        .name(name)
                        .build())
                .doOnError(error -> logger.error(String.format("Deleting application %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> map(String appName, String uri) {
        return cloudFoundryOperations
                .routes()
                .map(RouteRequestBuilder.buildMapRouteRequest(appName, uri))
                .doOnSubscribe(it -> logger.info(String.format("Mapping URL %s to application %s...", uri, appName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Mapping URL %s to application %s... OK", uri, appName)))
                .doOnError(error -> logger.error(String.format("\"Mapping URL %s to application %s... FAILED: %s", uri, appName, error.getMessage())));
    }

    @Override
    public Mono<Void> unmap(String appName, String uri) {
        return cloudFoundryOperations
                .routes()
                .unmap(RouteRequestBuilder.buildUnmapRouteRequest(appName, uri))
                .doOnSubscribe(subscription -> logger.info(String.format("Unmapping URL %s from application %s...", uri, appName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Unmapping URL %s from application %s... OK", uri, appName)))
                .doOnError(error -> logger.error(String.format("Unmapping URL %s from application %s... FAILED: %s", uri, appName, error.getMessage())));
    }


    public Mono<Void> renameApp(String appName, String newName) {
        return renameApp(appName, newName, true);
    }

    @Override
    public Mono<Void> renameApp(String appName, String newName, boolean failIfAppDoesNotExist) {
        return getApp(appName)
                .then(doRenameApp(appName, newName))
                .otherwise(e -> {
                    if(failIfAppDoesNotExist){
                        return Mono.error(e);
                    }
                    logger.info(String.format("Application %s does not exist. No rename will occur.", appName));
                    return Mono.empty();
                });
    }

    private Mono<Void> doRenameApp(String appName, String newName) {
        return cloudFoundryOperations
                .applications()
                .rename(RenameApplicationRequest.builder()
                        .name(appName)
                        .newName(newName)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Renaming application %s to %s...", appName, newName)))
                .doOnSuccess(aVoid -> logger.info(String.format("Renaming application %s to %s... OK", appName, newName)))
                .doOnError(error -> logger.error(String.format("Renaming application %s to %s... FAILED: %s", appName, newName, error.getMessage())));
    }

    @Override
    public Flux<ServiceInstance> services() {
        return cloudFoundryOperations
                .services()
                .listInstances()
                .doOnSubscribe(subscription -> logger.info("Getting services.."))
                .doOnComplete(() -> logger.info("Getting services.. OK"))
                .doOnError(throwable -> logger.error("Getting services... FAILED: " + throwable.getMessage()));
    }

    @Override
    public Mono<ServiceInstance> service(String name) {
        return getService(name)
                .doOnSubscribe(subscription -> logger.info(String.format("Getting service %s...", name)))
                .doOnSuccess(instance -> logger.info(String.format("Getting service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Getting service %s... FAILED: %s", name, error.getMessage())));
    }

    public Mono<ServiceInstance> getService(String name) {
        return cloudFoundryOperations
                .services()
                .getInstance(GetServiceInstanceRequest.builder()
                        .name(name)
                        .build());
    }

    @Override
    public Mono<Void> createService(String serviceInstanceName, String service, String plan, boolean failIfExists) {
        if(!failIfExists){
            return service(serviceInstanceName)
                    .doOnNext(instance -> logger.info(String.format("Service named %s already exists. No action will be taken.", serviceInstanceName)))
                    .then()
                    .otherwise(Exception.class, t -> createService(serviceInstanceName, service, plan));
        }
        return createService(serviceInstanceName, service, plan);
    }

    public Mono<Void> createService(String serviceInstanceName, String service, String plan) {
        ServiceConfiguration serviceConfiguration = ServiceConfiguration.builder()
                .serviceInstance(serviceInstanceName)
                .service(service)
                .plan(plan)
                .build();
        return createService(serviceConfiguration);
    }

    private Mono<Void> createService(ServiceConfiguration serviceConfiguration) {
        return cloudFoundryOperations
                .services()
                .createInstance(CreateServiceInstanceRequest.builder()
                        .serviceName(serviceConfiguration.getService())
                        .planName(serviceConfiguration.getPlan())
                        .serviceInstanceName(serviceConfiguration.getServiceInstance())
                        .parameters(serviceConfiguration.getParameters())
                        .tags(serviceConfiguration.getTags())
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Creating service %s...", serviceConfiguration.getServiceInstance())))
                .doOnSuccess(instance -> logger.info(String.format("Creating service %s... OK", serviceConfiguration.getServiceInstance())))
                .doOnError(error -> logger.error(String.format("Creating service %s... FAILED: %s", serviceConfiguration.getServiceInstance(), error.getMessage())));
    }

    public Mono<Void> updateService(ServiceConfiguration serviceConfiguration){
        return getService(serviceConfiguration.getServiceInstance())
                .then(serviceInstance -> Mono.when(
                        Mono.just(serviceConfiguration),
                        Mono.just(serviceInstance),
                        requestListServices()
                                .filter(serviceResource -> serviceResource.getEntity().getLabel().equals(serviceInstance.getService()))
                                .single()
                ))
                .flatMap(tuple -> getUpdateServiceInstanceRequest(tuple))
                .single()
                .then(updateServiceInstanceRequest -> cloudFoundryOperations
                        .services()
                        .updateInstance(updateServiceInstanceRequest))
                .doOnSubscribe(subscription -> logger.info(String.format("Updating service %s...", serviceConfiguration.getServiceInstance())))
                .doOnSuccess(service -> logger.info(String.format("Updating service %s... OK", serviceConfiguration.getServiceInstance())))
                .doOnError(error -> logger.error(String.format("Updating service %s... FAILED: %s", serviceConfiguration.getServiceInstance(), error.getMessage())));
    }

    private Flux<ServiceResource> requestListServices() {
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient
                        .services()
                        .list(ListServicesRequest.builder()
                                .build()));
    }

    private Mono<UpdateServiceInstanceRequest> getUpdateServiceInstanceRequest(Tuple3<ServiceConfiguration, ServiceInstance, ServiceResource> tuple) {
        return getUpdateServiceInstanceRequest(tuple.getT1(), tuple.getT2(), tuple.getT3().getEntity());
    }

    private Mono<UpdateServiceInstanceRequest> getUpdateServiceInstanceRequest(ServiceConfiguration serviceConfiguration, ServiceInstance serviceInstance,
                                                                         ServiceEntity serviceEntity) {
        if(serviceEntity.getPlanUpdateable()){
            return Mono.just(UpdateServiceInstanceRequest.builder()
                    .serviceInstanceName(serviceConfiguration.getServiceInstance())
                    .addAllTags(serviceConfiguration.getTags())
                    .parameters(serviceConfiguration.getParameters())
                    .planName(serviceConfiguration.getPlan())
                    .build());
        }
        String currentPlan = serviceInstance.getPlan();
        String requestedPlan = serviceConfiguration.getPlan();
        if(!currentPlan.equals(requestedPlan)){
            return Mono.error(new CloudFoundryServiceException(
                    String.format("A plan change from %s to %s was requested but the %s service does not support updating of plans",
                            currentPlan, requestedPlan, serviceEntity.getLabel())));
        }
        return Mono.just(UpdateServiceInstanceRequest.builder()
                .serviceInstanceName(serviceConfiguration.getServiceInstance())
                .addAllTags(serviceConfiguration.getTags())
                .parameters(serviceConfiguration.getParameters())
                .build());
    }

    @Override
    public Mono<Void> createUserProvidedService(String name, Map<String, Object> credentials, boolean ignoreIfExists) {
        if(ignoreIfExists){
            return getService(name)
                    .doOnNext(instance -> logger.info(String.format("Service named %s already exists. No action will be taken.", name)))
                    .then()
                    .otherwise(Exception.class, t -> createUserProvidedService(name, credentials));
        }
        return createUserProvidedService(name, credentials);
    }

    public Mono<Void> createUserProvidedService(String name, Map<String, Object> credentials) {
        return cloudFoundryOperations
                .services()
                .createUserProvidedInstance(CreateUserProvidedServiceInstanceRequest.builder()
                        .name(name)
                        .credentials(credentials)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Creating user provided service %s...", name)))
                .doOnSuccess(service -> logger.info(String.format("Creating user provided service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Creating user provided service %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> pushService(ServiceConfiguration serviceConfiguration) {
        return Mono.empty()
                .then(getService(serviceConfiguration.getServiceInstance())
                        .then(updateService(serviceConfiguration)))
                .otherwise(IllegalArgumentException.class, t -> createService(serviceConfiguration));
    }

    @Override
    public Mono<Void> deleteService(String name) {
        return cloudFoundryOperations
                .services()
                .deleteInstance(DeleteServiceInstanceRequest.builder()
                        .name(name)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting service %s...", name)))
                .doOnSuccess(it -> logger.info(String.format("Deleting service %s... OK", name)))
                .doOnError(error -> logger.error(String.format("Deleting service %s... FAILED: %s", name, error.getMessage())));
    }

    @Override
    public Mono<Void> bindService(String serviceName, String applicationName) {
        return cloudFoundryOperations
                .services()
                .bind(BindServiceInstanceRequest.builder()
                        .serviceInstanceName(serviceName)
                        .applicationName(applicationName)
                        .build())
                .doOnSubscribe(it -> logger.info(String.format("Binding service %s to app %s...", serviceName, applicationName)))
                .doOnSuccess(it -> logger.info(String.format("Binding service %s to app %s... OK", serviceName, applicationName)))
                .doOnError(error -> logger.error(String.format("Binding service %s to app %s... FAILED: %s", serviceName, applicationName, error.getMessage())));
    }

    @Override
    public Mono<Void> unbindService(String serviceName, String applicationName) {
        return cloudFoundryOperations
                .services()
                .unbind(UnbindServiceInstanceRequest.builder()
                        .serviceInstanceName(serviceName)
                        .applicationName(applicationName)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Unbinding service %s from app %s...", serviceName, applicationName)))
                .doOnSuccess(it -> logger.info(String.format("Unbinding service %s from app %s... OK", serviceName, applicationName)))
                .doOnError(error -> logger.error(String.format("Unbinding service %s from app %s... FAILED: %s", serviceName, applicationName, error.getMessage())));
    }

    @Override
    public Mono<Void> addDomain(String domain, String organization) {
        return cloudFoundryOperations
                .domains()
                .create(CreateDomainRequest.builder()
                        .domain(domain)
                        .organization(organization)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Adding private domain %s to organization %s...", domain, organization)))
                .doOnSuccess(it -> logger.info(String.format("Adding private domain %s to organization %s... OK", domain, organization)))
                .doOnError(error -> logger.error(String.format("Adding private domain %s to organization %s.... FAILED: %s", domain, organization, error.getMessage())));
    }

    @Override
    public Mono<Void> deleteDomain(String domainName) {
        return getDomainId(domainName)
                .otherwiseIfEmpty(Mono.error(new IllegalArgumentException("Domain " + domainName + " does not exist.")))
                .then(domainId -> doDeleteDomain(domainId))
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting domain %s...", domainName)))
                .doOnSuccess(it -> logger.info(String.format("Deleting domain %s... OK", domainName)))
                .doOnError(error -> logger.error(String.format("Deleting domain %s.... FAILED: %s", domainName, error.getMessage())));
    }

    private Mono<String> getDomainId(String domainName){
        return cloudFoundryOperations
                .domains()
                .list()
                .filter(domain -> domain.getName().equals(domainName))
                .map(d -> d.getId())
                .singleOrEmpty();
    }

    private Mono<Void> doDeleteDomain(String domainId){
        return cloudFoundryClient
                .domains()
                .delete(DeleteDomainRequest.builder()
                        .domainId(domainId)
                        .build())
                .then();
    }

    @Override
    public Mono<Void> createRoute(String domain, String host, String path, String space) {
        return cloudFoundryOperations
                .routes()
                .create(CreateRouteRequest.builder()
                        .domain(domain)
                        .host(host)
                        .path(path)
                        .space(space)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Adding route %s.%s%s...", host, domain, path)))
                .doOnSuccess(it -> logger.info(String.format("Adding route %s.%s%s... OK", host, domain, path)))
                .doOnError(error -> logger.error(String.format("Adding route %s.%s%s... FAILED: %s", host, domain, path, error.getMessage())));
    }

    @Override
    public Mono<Void> deleteRoute(String domain, String host, String path) {
        return cloudFoundryOperations
                .routes()
                .delete(DeleteRouteRequest.builder()
                        .domain(domain)
                        .host(host)
                        .path(path)
                        .build())
                .doOnSubscribe(subscription -> logger.info(String.format("Deleting route %s.%s%s...", host, domain, path)))
                .doOnSuccess(it -> logger.info(String.format("Deleting route %s.%s%s... OK", host, domain, path)))
                .doOnError(error -> logger.error(String.format("Deleting route %s.%s%s... FAILED: %s", host, domain, path, error.getMessage())));
    }
}
