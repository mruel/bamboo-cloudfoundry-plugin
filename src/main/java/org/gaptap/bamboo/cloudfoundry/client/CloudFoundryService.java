/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.v2.info.GetInfoResponse;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.cloudfoundry.operations.services.ServiceInstance;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.util.Map;

/**
 * @author David Ehringer
 */
public interface CloudFoundryService {

    Mono<Void> validateConnection();

    Mono<GetInfoResponse> info();

    Flux<ApplicationSummary> apps();

    Mono<ApplicationDetail> app(String name);

    /**
     * A version of  Mono<ApplicationDetail> app(String name) with no logging
     */
    Mono<ApplicationDetail> getApp(String name);

    Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, boolean start);

    Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, int secondsToWait);

    Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, File application, int secondsToWait, BlueGreenConfiguration blueGreenConfiguration);

    Mono<Void> startApp(String name);

    Mono<Void> startApp(String name, int secondsToWait);

    Mono<Void> stopApp(String name);

    Mono<Void> restartApp(String name);

    Mono<Void> deleteApp(String name);

    Mono<Void> map(String appName, String uri);

    Mono<Void> unmap(String appName, String uri);

    Mono<Void> renameApp(String appName, String newName, boolean failIfAppDoesNotExist);

    Flux<ServiceInstance> services();

    Mono<ServiceInstance> service(String name);

    /**
     * A version of Mono<ServiceInstance> service(String name) with no logging
     */
    Mono<ServiceInstance> getService(String name);

    Mono<Void> createService(String serviceInstanceName, String service, String plan, boolean failIfExists);

    Mono<Void> createUserProvidedService(String name, Map<String, Object> credentials, boolean ignoreIfExists);

    Mono<Void> pushService(ServiceConfiguration serviceConfiguration);

    Mono<Void> deleteService(String name);

    Mono<Void> bindService(String serviceName, String applicationName);

    Mono<Void> unbindService(String serviceName, String applicationName);

    Mono<Void> addDomain(String domain, String organization);

    Mono<Void> deleteDomain(String domain);

    Mono<Void> createRoute(String domain, String host, String path, String space);

    Mono<Void> deleteRoute(String domain, String host, String path);
}
