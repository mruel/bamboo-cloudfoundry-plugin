package org.gaptap.bamboo.cloudfoundry.client;

import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

@Value.Immutable
abstract class _ConnectionParameters {

    abstract String getTargetUrl();

    abstract String getUsername();

    abstract String getPassword();

    @Nullable
    abstract String getProxyHost();

    @Nullable
    abstract Integer getProxyPort();

    abstract Boolean isTrustSelfSignedCerts();

}
