package org.gaptap.bamboo.cloudfoundry.client;

import org.gaptap.bamboo.cloudfoundry.Nullable;
import org.immutables.value.Value;

import java.util.List;
import java.util.Map;

@Value.Immutable
abstract class _ApplicationConfiguration {

    public static final int DEFAULT_DISK_QUOTA = 1024;
    public static final Integer PLATFORM_DEFAULT_TIMEOUT = null;
    public static final int DEFAULT_INSTANCE_COUNT = 1;
    public static final Integer DEFAULT_MEMORY = 1024;

    abstract String name();

    @Nullable
    abstract Integer memory();

    @Nullable
    abstract Integer diskQuota();

    @Nullable
    abstract Integer instances();

    @Nullable
    abstract String command();

    @Nullable
    abstract String buildpackUrl();

    @Nullable
    abstract String stack();

    abstract Map<String, String> environment();

    abstract List<String> routes();

    // TODO I think this is actually called "health check timeout"
    @Nullable
    abstract Integer startTimeout();

    abstract List<String> serviceBindings();

    @Nullable
    abstract String healthCheckType();

    @Nullable
    abstract Boolean noRoute();

    public void logConfiguration(Logger logger){
        logger.info("--- Specified Application Configuration ---");
        logger.info("name: " + name());
        logger.info("instances: " + valueOrEmpty(instances()));
        logger.info("memory: " + valueOrEmpty(memory()) + "M");
        logger.info("diskQuota: " + valueOrEmpty(diskQuota()) + "M");
        logger.info("buildpack: " + valueOrEmpty(buildpackUrl()));
        logger.info("stack: " + valueOrEmpty(stack()));
        logger.info("health check type: " + valueOrEmpty(healthCheckType()));
        logger.info("health check timeout: " + valueOrEmpty(startTimeout()));
        logger.info("routes: " + valueOrEmpty(routes()));
        logger.info("no-route: " + valueOrEmpty(noRoute()));
        logger.info("bound services: " + valueOrEmpty(serviceBindings()));
        logger.info("environment: ");
        for(Map.Entry entry: environment().entrySet()){
            logger.info("  - " + entry.getKey() + "=" + "[REDACTED]");
        }
    }

    private String valueOrEmpty(Object field) {
        return field == null ? "" :String.valueOf(field);
    }

}
