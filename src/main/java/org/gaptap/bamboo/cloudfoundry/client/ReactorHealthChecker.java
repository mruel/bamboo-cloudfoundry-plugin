package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.reactor.util.DefaultSslCertificateTruster;
import org.cloudfoundry.reactor.util.SslCertificateTruster;
import org.cloudfoundry.reactor.util.StaticTrustManagerFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.config.ClientOptions;
import reactor.ipc.netty.config.HttpClientOptions;
import reactor.ipc.netty.http.HttpClient;

import java.time.Duration;
import java.util.Optional;

public class ReactorHealthChecker implements HealthChecker {

    private HttpClient client;
    private final SslCertificateTruster truster;

    public ReactorHealthChecker() {
        ClientOptions options = HttpClientOptions.create()
                .sslSupport();
        truster =  new DefaultSslCertificateTruster(Optional.empty());
        options.ssl().trustManager(new StaticTrustManagerFactory(truster));
        client = HttpClient.create(options);
    }

    public void setHttpClient(HttpClient httpClient) {
        this.client = httpClient;
    }

    @Override
    public Mono<Void> assertHealthy(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration, Logger logger) {
        assertRoutesMapped(applicationConfiguration);
        UriComponents uriComponents = UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(applicationConfiguration.routes().get(0))
                .port(443)
                .path(blueGreenConfiguration.healthCheckEndpoint())
                .build();
        configureSsl(blueGreenConfiguration, uriComponents);
        return client
                .get(uriComponents.toUriString())
                .doOnSubscribe(subscription -> logger.info("Checking health of " + uriComponents.toUriString()))
                .doOnError(throwable -> logger.error("Health check failed: " + throwable.getMessage()))
                .map(httpClientResponse -> httpClientResponse.status().code())
                .map(responseCode -> assertSuccess(responseCode, logger))
                .then();
    }

    private void assertRoutesMapped(ApplicationConfiguration applicationConfiguration) {
        if(applicationConfiguration.routes().isEmpty()){
            throw new IllegalArgumentException("A health check cannot be performed on an application with no mapped routes.");
        }
    }

    private void configureSsl(BlueGreenConfiguration blueGreenConfiguration, UriComponents uriComponents) {
        if(blueGreenConfiguration.skipSslValidation()) {
            truster.trust(uriComponents.getHost(), uriComponents.getPort(), Duration.ofSeconds(30));
        }
    }

    private int assertSuccess(int responseCode, Logger logger){
        if(200 != responseCode){
            logger.error("Health check failed. HTTP response code: " + responseCode);
            throw new HealthCheckException();
        }
        logger.info("Health check succeeded.");
        return responseCode;
    }
}
