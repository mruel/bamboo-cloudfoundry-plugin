
[@ui.bambooSection titleKey='cloudfoundry.task.global.environment.section']

	[@ww.select labelKey='cloudfoundry.task.global.environment.configOptions' name='cf_environment_config_option'
                                        listKey='key' listValue='value' toggle='true'
                                        list=cf_environment_config_options /]

    [@ui.bambooSection dependsOn='cf_environment_config_option' showOn='shared']

		[#if deploymentMode]
			[#if cf_targets.size() == 0]
				[@ui.messageBox type="warning" titleKey="cloudfoundry.task.global.target.none.configured.title"]
					[@ww.text name="cloudfoundry.task.global.target.none.configured.message"]
					[/@ww.text]&nbsp;
					[#if fn.hasAdminPermission()]
						[@ww.text name="cloudfoundry.task.global.target.none.configured.add"]
							[@ww.param]${req.contextPath}/admin/cloudfoundry/configuration.action[/@ww.param]
						[/@ww.text]
					[/#if]
				[/@ui.messageBox]
			[/#if]
			[@ww.select labelKey="cloudfoundry.task.global.target" name="cf_targetId" required="true" list=cf_targets listKey="key" listValue="value" toggle='true' /]
		[#else]
			[#if cf_build_targets.size() == 0]
				[@ui.messageBox type="warning" titleKey="cloudfoundry.task.global.target.none.configured.build_targets.title"]
					[@ww.text name="cloudfoundry.task.global.target.none.configured.message"]
					[/@ww.text]&nbsp;
					[#if fn.hasAdminPermission()]
						[@ww.text name="cloudfoundry.task.global.target.none.configured.add"]
							[@ww.param]${req.contextPath}/admin/cloudfoundry/configuration.action[/@ww.param]
						[/@ww.text]
					[/#if]
				[/@ui.messageBox]
			[/#if]
			[@ww.select labelKey="cloudfoundry.task.global.target" name="cf_targetId" required="true" list=cf_build_targets listKey="key" listValue="value" toggle='true' /]
		[/#if]

    [/@ui.bambooSection]

	[@ui.bambooSection dependsOn='cf_environment_config_option' showOn='task_level']

		[@ww.textfield labelKey='cloudfoundry.target.url' descriptionKey='cloudfoundry.target.url.description' name='cf_environment_url' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.credentials.username' descriptionKey='cloudfoundry.credentials.username.description' name='cf_environment_username' required='true' /]
		[#if context.get("cf_environment_password_enc")?has_content]
		    [@ww.checkbox labelKey='cloudfoundry.admin.password.change' toggle=true name='passwordChange' /]
            [@ui.bambooSection dependsOn='passwordChange' showOn=true]
            [@ww.password labelKey="cloudfoundry.credentials.password" name="cf_environment_password" required="true" /]
            [/@ui.bambooSection]
        [#else]
            [@ww.hidden name='passwordChange' value=true /]
            [@ww.password labelKey="cloudfoundry.credentials.password" name="cf_environment_password" required="true" /]
        [/#if]

		[@ui.bambooSection titleKey='cloudfoundry.target.advanced' collapsible=true]
			[@ww.checkbox labelKey='cloudfoundry.target.trustSelfSignedCerts' descriptionKey='cloudfoundry.target.trustSelfSignedCerts.description' name='cf_environment_trustSelfSignedCerts' /]
			[@ww.textfield labelKey='cloudfoundry.proxy.host' descriptionKey='cloudfoundry.proxy.host.description' name='cf_environment_proxy_host' /]
			[@ww.textfield labelKey='cloudfoundry.proxy.port' descriptionKey='cloudfoundry.proxy.port.description' name='cf_environment_proxy_port' /]
		[/@ui.bambooSection]

	[/@ui.bambooSection]

[/@ui.bambooSection]

[@ui.bambooSection titleKey='cloudfoundry.task.global.target.section']

	[@ww.textfield labelKey='cloudfoundry.task.global.org' name='cf_org' required='true' /]
	[@ww.textfield labelKey='cloudfoundry.task.global.space' name='cf_space' required='true' /]

[/@ui.bambooSection]